DOCKER_NS ?= guardianproject-ops/docker-amigo-cypress
UPSTREAM ?= docker.io/cypress/browsers
TAGS ?= $(shell python upstream-tags.py| jq -r '.tags[]')

list-tags:
	@echo "$(TAGS)"

build: $(TAGS)

$(TAGS):
	@echo "Building from $(UPSTREAM):$@"
	docker build --pull --no-cache --build-arg CY_IMAGE=$(UPSTREAM):$@ -t ${DOCKER_NS}:$@ $(PWD)
	docker push ${DOCKER_NS}:$@
