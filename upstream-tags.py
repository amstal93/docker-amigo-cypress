import requests
import json
from datetime import datetime

prefixes = [
        "node12",
        "node14",
        "node16",
        ]

#####

def find_first(tags, prefix):
    for tag in tags:
        name = tag["name"]
        updated = tag["updated"]
        if name.startswith(prefix):
            return tag
    return None


repo_tag_url = 'https://hub.docker.com/v2/repositories/cypress/browsers/tags'
results = requests.get(repo_tag_url).json()['results']

tags = []
for repo in results:
    ts = datetime.strptime(repo['last_updated'], "%Y-%m-%dT%H:%M:%S.%fZ")
    tags.append({"name": repo["name"], "updated": ts})


tags_sorted = sorted(tags, key=lambda k: k["updated"], reverse=True)


found = []


for prefix in prefixes:
    tag = find_first(tags_sorted, prefix)
    if tag:
        found.append(tag)


output = json.dumps({
    "tags": [t["name"] for t in found]
    })

print(output)
